package com.stock.db;

import java.util.ArrayList;

import com.stock.db.Stock;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class StockModel extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "stocksdb";
	private static final String STOCK_TABLE = "stocks";
	private static final String STOCK_ID = "id";
	private static final String STOCK_DATE = "date";
	private static final String STOCK_CODE = "code";
	private static final String STOCK_PRICE = "price";
	private static final String STOCK_ENTRY_PRICE = "entry_price";
	private static final String STOCK_TARGET_PRICE = "target_price";
	private static final String STOCK_CUTLOSS_PRICE = "cutloss_price";
	private static final String STOCK_SHARES = "shares";

	public StockModel(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String query = "CREATE TABLE " + STOCK_TABLE + "("
				+ STOCK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ STOCK_DATE + " TEXT,"
				+ STOCK_CODE + " TEXT NOT NULL,"
				+ STOCK_PRICE + " REAL,"
				+ STOCK_ENTRY_PRICE + " REAL,"
				+ STOCK_TARGET_PRICE + " REAL,"
				+ STOCK_CUTLOSS_PRICE + " REAL,"
				+ STOCK_SHARES + " INT" + ")";
		db.execSQL(query);

		/*
		// Initial values
		ContentValues values = new ContentValues();
		//values.put(STOCK_ID, 0);
		//values.put(STOCK_DATE, "1970-01-01 00:00:00");
		values.put(STOCK_CODE, "NUL");
		values.put(STOCK_PRICE, 0.00);
		values.put(STOCK_ENTRY_PRICE, 0.00);
		values.put(STOCK_TARGET_PRICE, 0.00);
		values.put(STOCK_CUTLOSS_PRICE, 0.00);
		values.put(STOCK_SHARES, 0);

		db.insert(STOCK_TABLE, null, values);
		*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		//db.execSQL("DROP TABLE IF EXISTS " + SMS_TABLE);
		// Create tables again
		//onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	public void addStock(Stock stock) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		//values.put(STOCK_ID, 0);
		//values.put(STOCK_DATE, "1970-01-01 00:00:00");
		values.put(STOCK_CODE, stock.getCode());
		values.put(STOCK_PRICE, stock.getPrice());
		values.put(STOCK_ENTRY_PRICE, stock.getEntryPrice());
		values.put(STOCK_TARGET_PRICE, stock.getTargetPrice());
		values.put(STOCK_CUTLOSS_PRICE, stock.getCutlossPrice());
		values.put(STOCK_SHARES, stock.getShares());

		db.insert(STOCK_TABLE, null, values);
	}

	/*
	Stock getStock(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(STOCK_TABLE, new String[] { STOCK_ID,
				STOCK_DATE, STOCK_CODE }, STOCK_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Stock stock = new Stock();
		stock.setID(Integer.parseInt(cursor.getString(0)));
		stock.setDate(cursor.getString(1));
		stock.setCode(cursor.getString(2));
		stock.setPrice(Double.valueOf(cursor.getString(3)));
		stock.setEntryPrice(Double.valueOf(cursor.getString(4)));
		stock.setTargetPrice(Double.valueOf(cursor.getString(5)));
		stock.setCutlossPrice(Double.valueOf(cursor.getString(6)));
		stock.setShares(Long.parseLong(cursor.getString(7)));

		return stock;
	}
	*/
	public Stock getStockByCode(String code) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(STOCK_TABLE, new String[] {"*"}, STOCK_CODE + "=?",
				new String[] { code }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Stock stock = new Stock();
		stock.setID(Integer.parseInt(cursor.getString(0)));
		stock.setDate(cursor.getString(1));
		stock.setCode(cursor.getString(2));
		stock.setPrice(Double.valueOf(cursor.getString(3)));
		stock.setEntryPrice(Double.valueOf(cursor.getString(4)));
		stock.setTargetPrice(Double.valueOf(cursor.getString(5)));
		stock.setCutlossPrice(Double.valueOf(cursor.getString(6)));
		stock.setShares(Long.parseLong(cursor.getString(7)));

		return stock;
	}
	
	public ArrayList<Stock> getAllStocks() {
		ArrayList<Stock> stockList = new ArrayList<Stock>();
		SQLiteDatabase db = this.getWritableDatabase();

		String query = "SELECT  * FROM " + STOCK_TABLE;
		Cursor cursor = db.rawQuery(query, null);

		if (cursor.moveToFirst()) {
			do {
				Stock stock = new Stock();
				stock.setID(Integer.parseInt(cursor.getString(0)));
				stock.setDate(cursor.getString(1));
				stock.setCode(cursor.getString(2));
				stock.setPrice(Double.valueOf(cursor.getString(3)));
				stock.setEntryPrice(Double.valueOf(cursor.getString(4)));
				stock.setTargetPrice(Double.valueOf(cursor.getString(5)));
				stock.setCutlossPrice(Double.valueOf(cursor.getString(6)));
				stock.setShares(Long.parseLong(cursor.getString(7)));

				stockList.add(stock);
				
			} while (cursor.moveToNext());
		}

		return stockList;
	}

	public void updateStock(Stock stock) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		//values.put(STOCK_DATE, stock.getDate());
		values.put(STOCK_CODE, stock.getCode());
		//values.put(STOCK_PRICE, stock.getPrice());
		values.put(STOCK_ENTRY_PRICE, stock.getEntryPrice());
		values.put(STOCK_TARGET_PRICE, stock.getTargetPrice());
		values.put(STOCK_CUTLOSS_PRICE, stock.getCutlossPrice());
		values.put(STOCK_SHARES, stock.getShares());

		//return db.update(STOCK_TABLE, values, STOCK_ID + " = ?",
		//		new String[] { String.valueOf(stock.getID()) });
		db.update(STOCK_TABLE, values, STOCK_ID + " = ?",
				new String[] { String.valueOf(stock.getID()) });
		//db.close();
	}

	public void deleteStock(Stock stock) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(STOCK_TABLE, STOCK_ID + " = ?",
				new String[] { String.valueOf(stock.getID()) });
		//db.close();
	}

	/*
	public int getStockCount() {
		SQLiteDatabase db = this.getReadableDatabase();

		String query = "SELECT * FROM " + STOCK_TABLE;
		Cursor cursor = db.rawQuery(query, null);
		cursor.close();

		return cursor.getCount();
	}
	*/

}
