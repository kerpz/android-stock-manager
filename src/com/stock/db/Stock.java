package com.stock.db;

import android.os.Parcel;
import android.os.Parcelable;

public class Stock implements Parcelable {
	
	int id;
	String date;
	String code;
	double price;
	double entry_price;
	double target_price;
	double cutloss_price;
	long shares;
	
	public Stock() {
	}

	private Stock(Parcel in) {
		this.id = in.readInt();
		this.date = in.readString();
		this.code = in.readString();
		this.price = in.readDouble();
		this.entry_price = in.readDouble();
		this.target_price = in.readDouble();
		this.cutloss_price = in.readDouble();
		this.shares = in.readLong();
	}

	public int getID() {
		return this.id;
	}
	public void setID(int id) {
		this.id = id;
	}
	
	public String getDate() {
		return this.date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getCode() {
		return this.code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public double getPrice() {
		return this.price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getEntryPrice() {
		return this.entry_price;
	}
	public void setEntryPrice(double price) {
		this.entry_price = price;
	}
	
	public double getTargetPrice() {
		return this.target_price;
	}
	public void setTargetPrice(double price) {
		this.target_price = price;
	}
	
	public double getCutlossPrice() {
		return this.cutloss_price;
	}
	public void setCutlossPrice(double price) {
		this.cutloss_price = price;
	}
	
	public long getShares() {
		return this.shares;
	}
	public void setShares(long shares) {
		this.shares = shares;
	}
	
	// col fees and charges https://www.colfinancial.com/ape/final2/home/faq.asp
	double getBasicCharge(long shares, double total_price) {
		//double total_price = shares * price;
		double commission = total_price * 0.0025;
		commission = (commission > 20.0000) ? commission : 20.0000;
		double vat = commission * 0.12;
		double pse_fee = total_price * 0.00005;
		double sccp = total_price * 0.0001;

		return (commission + vat + pse_fee + sccp);
	}
	double getNetBuy() {
		double buy_total = this.shares * this.entry_price; 
		double buy_charges = getBasicCharge(this.shares, buy_total);

		return (buy_total + buy_charges);
	}
	double getNetSell(double sell_price) {
		double sell_total = this.shares * sell_price; 
		double sales_tax = sell_total * 0.006;
		double sell_charges = getBasicCharge(this.shares, sell_total) + sales_tax;

		return (sell_total - sell_charges);
	}

	public double getBuyAvg() {
		return getNetBuy() / this.shares;
	}
	double getChangeTotal(double sell_price) {
		double result = 0.0000;
		if (sell_price > 0.0000) {
			result = (getNetSell(sell_price) - getNetBuy());
		}
		return result;
	}
	double getChangePercent(double sell_price) {
		double result = 0.00;
		if (sell_price > 0.00) {
			double buy_net = getNetBuy();
			double sell_net = getNetSell(sell_price);
			double change_base_price = sell_net - buy_net;
			result = ((change_base_price / buy_net) * 100);
		}
		return result;
	}
	
	
	public double getTargetPercent() {
		return getChangePercent(this.target_price);
	}
	public double getTargetTotal() {
		return getChangeTotal(this.target_price);
	}
	public double getCutlossPercent() {
		return getChangePercent(this.cutloss_price);
	}
	public double getCutlossTotal() {
		return getChangeTotal(this.cutloss_price);
	}
	public double getChangePercent() {
		return getChangePercent(this.price);
	}
	public double getChangeTotal() {
		return getChangeTotal(this.price);
	}

	@Override
	public String toString() {
		return "Stock [id=" + id
				+ ", date=" + date
				+ ", code=" + code
				+ ", price=" + price
				+ ", entry_price=" + entry_price
				+ ", target_price=" + target_price
				+ ", cutloss_price=" + cutloss_price
				+ ", shares=" + shares
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stock other = (Stock) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(getID());
		parcel.writeString(getDate());
		parcel.writeString(getCode());
		parcel.writeDouble(getPrice());
		parcel.writeDouble(getEntryPrice());
		parcel.writeDouble(getTargetPrice());
		parcel.writeDouble(getCutlossPrice());
		parcel.writeLong(getShares());
	}

	public static final Parcelable.Creator<Stock> CREATOR = new Parcelable.Creator<Stock>() {
		public Stock createFromParcel(Parcel in) {
			return new Stock(in);
		}

		public Stock[] newArray(int size) {
			return new Stock[size];
		}
	};

}
