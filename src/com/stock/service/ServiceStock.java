package com.stock.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;

import com.stock.db.Stock;
import com.stock.db.StockModel;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ServiceStock extends Service {
	//private static final String TAG = "SERVICEPULLDATA";
	Context activity;
	public static ArrayList<Stock> stocks;
	
	static Handler handler;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);

		return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        //Log.e(TAG, "onCreate");
    	Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();
        super.onCreate();
		activity = getBaseContext();
		handler = new Handler();

		StockModel db = new StockModel(activity);
		stocks = db.getAllStocks();
		db.close();
		//new GetDevTask().execute();
		new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks.json");
	}

    @Override
    public void onDestroy() {
        //Log.e(TAG, "onDestroy");
    	Toast.makeText(this, "Service destroyed", Toast.LENGTH_LONG).show();
    	handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    public ArrayList<Stock> getStocks() {
        return stocks;
    }

    class GetDevTask extends AsyncTask<Void, Void, ArrayList<Stock>> {
		@Override
		protected ArrayList<Stock> doInBackground(Void... arg0) {
			StockModel db = new StockModel(activity);
			ArrayList<Stock> stockList = db.getAllStocks();
			db.close();
			return stockList;
		}

		@Override
		protected void onPostExecute(ArrayList<Stock> stockList) {
			if (stocks == null || stocks.size() != stockList.size()) {
				stocks = stockList;
			}
			if (stocks.size() != 0 && stocks != null) {
				new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks.json");
			} else {
				Toast.makeText(activity, "No Device Found @ DB", Toast.LENGTH_SHORT).show();
			}
		}
	}

	class GetStockDataTask extends AsyncTask<String, Void, String> {
	    protected String doInBackground(String... args) {
            StringBuffer response = new StringBuffer();
	        try {
	            URL url = new URL(args[0]);

	            if (args[0].startsWith("https://")) {
		            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

		            // Create the SSL connection
		            SSLContext sc;
		            sc = SSLContext.getInstance("TLS");
		            //sc.init(null, null, new java.security.SecureRandom());
		            sc.init(
		            	null,
	            		new TrustManager[] { // accept all
							new X509TrustManager() {
								public void checkClientTrusted(X509Certificate[] chain, String authType) {}
								public void checkServerTrusted(X509Certificate[] chain, String authType) {}
								public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[]{}; }
							}
	            		}, 
	            		null
		            );
		            conn.setSSLSocketFactory(sc.getSocketFactory());
		            conn.setHostnameVerifier(new HostnameVerifier() {
						@Override
						public boolean verify(String hostname, SSLSession session) {
							// accept all
							return true;
						}
		            });
		            //HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		            conn.setRequestMethod("GET");

		            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            String inputLine;

		            while ((inputLine = in.readLine()) != null) {
		                response.append(inputLine);
		            }
		            in.close();
	            }
	            else {
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		            conn.setRequestMethod("GET");

		            //responseCode = conn.getResponseCode();
		            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            String inputLine;

		            while ((inputLine = in.readLine()) != null) {
		                response.append(inputLine);
		            }
		            in.close();
	            }
	        } catch (Exception e) {
	            //this.exception = e;
	            e.printStackTrace();
	        }
        	return response.toString();
	    }
	    protected void onPostExecute(String data) {
	    	if (data != null) {
	    		try {
		    		JSONObject obj_data = new JSONObject(data);
		    		
		    		//String as_of = obj_data.getString("as_of");

		    		JSONArray arr_stocks  = obj_data.getJSONArray("stock");
		    		//JSONObject obj_stock = arr_stocks.getJSONObject(0);

		    		boolean beep_flag = false;
					for (int i = 0; i < arr_stocks.length(); i++) {
						JSONObject obj_stock = arr_stocks.getJSONObject(i);
			    		//String name = obj_stock.getString("name");

						JSONObject obj_price = obj_stock.getJSONObject("price");
			    		String amount = obj_price.getString("amount");
			    		//String percent_change = obj_stock.getString("percent_change");
			    		//String volume = obj_stock.getString("volume");
			    		String symbol = obj_stock.getString("symbol");

			    		//String value = result.getString("value");

						for (Stock stock : stocks) {
							//new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks/" + stock.getCode() + ".json");
							if (stock.getCode().equals(symbol)) {
								//Toast.makeText(activity, String.format("%.2f", stock.getPrice()), Toast.LENGTH_SHORT).show();
					    		stock.setPrice(Double.valueOf(amount));

					    		if (stock.getPrice() > 0.0) {
						        	if (stock.getPrice() >= stock.getTargetPrice() && stock.getTargetPrice() > 0.0) {
						        		beep_flag = true;
						        	}
						        	if (stock.getPrice() <= stock.getCutlossPrice() && stock.getCutlossPrice() > 0.0) {
						        		beep_flag = true;
						        	}
					        	}
							}
						}
					}

					Toast.makeText(activity, "refresh service", Toast.LENGTH_SHORT).show();
		    		if (beep_flag) {
			        	try {
			        	    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			        	    Ringtone r = RingtoneManager.getRingtone(activity.getApplicationContext(), notification);
			        	    r.play();
			        	} catch (Exception e) {
			        	    e.printStackTrace();
			        	}					        
		    		}

		    		//performGlobalAction(GLOBAL_ACTION_BACK);
					Intent intent = new Intent("com.stock.status.action.REFRESH");
		            //intent.putExtra("text", text);
					sendBroadcast(intent);

					//contentAdapter = new ContentAdapter(recyclerView.getContext(), stocks);
			        //recyclerView.setLayoutManager(new LinearLayoutManager(activity));
			        //recyclerView.setAdapter(contentAdapter);
					//stockListAdapter.notifyDataSetChanged();
					//contentAdapter.notifyDataSetChanged();

			        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
			    	//int unit = Integer.valueOf(sharedPrefs.getString("pref_query_method", "1"));
			        

					final int duration = Integer.valueOf(sharedPrefs.getString("pref_refresh_duration", "5")) * 1000;
					//final Handler handler = new Handler();
					if (duration > 0) {
						handler.removeCallbacksAndMessages(null);
					    handler.postDelayed(new Runnable() {
					        public void run() {
					        	new GetDevTask().execute();
					        }
					    }, duration);
					}

	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	    	}
	    }
	}

}
