package com.stock.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;

import com.stock.db.Stock;
import com.stock.db.StockModel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Provides UI for the view with Cards.
 * https://codeburst.io/android-swipe-menu-with-recyclerview-8f28a235ff28
 */
public class FragmentCardStocks extends Fragment {
	static Activity activity;
	RecyclerView recyclerView;
	ArrayList<Stock> stocks;

	ContentAdapter contentAdapter;
	Stock stock;

	//private GetDevTask task;
	private ProgressDialog progressDialog;
	private FloatingActionButton fab;

	static Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = getActivity();
        handler = new Handler();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		handler.removeCallbacksAndMessages(null);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_card, container, false);

		recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

		fab = (FloatingActionButton) view.findViewById(R.id.fab);
	    fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//Bundle arguments = new Bundle();
				DialogFragmentStock customDevDialogFragment = new DialogFragmentStock();
				//customDevDialogFragment.setArguments(arguments);
				customDevDialogFragment.show(getFragmentManager(), "Add");
			}
		});

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity,
                recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                //Toast.makeText(activity, "Single Click on position :"+position,
                //        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
            	stock = stocks.get(position);
                //Toast.makeText(activity, "Long press on position :"+position,
                //        Toast.LENGTH_LONG).show();
                view.showContextMenu();
            }
        }));

        return view;
	}

	public static interface ClickListener{
		public void onClick(View view,int position);
		public void onLongClick(View view,int position);
	}

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
	    //menu.setHeaderTitle("Choose what to do");
	    menu.add(0, 0, 0, "Edit");
	    menu.add(0, 1 ,1, "Delete");
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
	    if (getUserVisibleHint()) {
            //SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
	    	//SmsManager smsManager = SmsManager.getDefault();

		    switch (menuItem.getItemId()) {
		        case 0:
		        	// Edit
					Bundle arguments = new Bundle();
					arguments.putParcelable("selectedItem", stock);
					DialogFragmentStock customDevDialogFragment = new DialogFragmentStock();
					customDevDialogFragment.setArguments(arguments);
					//customDevDialogFragment.show(getFragmentManager(), DialogFragmentEditStock.ARG_ITEM_ID);
					customDevDialogFragment.show(getFragmentManager(), "Edit");
		            break;
		        case 1:
		        	// Delete
					new AlertDialog.Builder(activity)
					//.setTitle("Title")
					.setMessage("Are you sure you want to delete?")
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							StockModel db = new StockModel(activity);
							db.deleteStock(stock);
							db.close();

							stocks.remove(stock);
							contentAdapter.notifyDataSetChanged();
						}})
					.setNegativeButton(android.R.string.no, null).show();
		            break;
		    }
		    return true;
	    }
	    return false;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			progressDialog = ProgressDialog.show(activity, "", "Loading data ...", true);
			progressDialog.setCancelable(true);

			updateView();
		}
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		CardView card;
		TextView textViewCode;
		TextView textViewPrice;
		TextView textViewInfo;
		TextView textViewTargetPrice;
		TextView textViewTargetPercent;
		TextView textViewCutlossPrice;
		TextView textViewCutlossPercent;
		TextView textViewChangePrice;
		TextView textViewChangePercent;

		ViewHolder(LayoutInflater inflater, ViewGroup parent) {
			super(inflater.inflate(R.layout.item_card, parent, false));
			card = (CardView) itemView.findViewById(R.id.card_view);
			
			textViewCode = (TextView) itemView.findViewById(R.id.textViewCode);
			textViewPrice = (TextView) itemView.findViewById(R.id.textViewPrice);
			textViewInfo = (TextView) itemView.findViewById(R.id.textViewInfo);
			textViewTargetPrice = (TextView) itemView.findViewById(R.id.textViewTargetPrice);
			textViewTargetPercent = (TextView) itemView.findViewById(R.id.textViewTargetPercent);
			textViewCutlossPrice = (TextView) itemView.findViewById(R.id.textViewCutlossPrice);
			textViewCutlossPercent = (TextView) itemView.findViewById(R.id.textViewCutlossPercent);
			textViewChangePrice = (TextView) itemView.findViewById(R.id.textViewChangePrice);
			textViewChangePercent = (TextView) itemView.findViewById(R.id.textViewChangePercent);
		}
    }

    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
		Context context;
		ArrayList<Stock> stocks;
		
		public ContentAdapter(Context context, ArrayList<Stock> stocks) {
			this.context = context;
			this.stocks = stocks;
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			context = parent.getContext();
			return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
		}

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
			Stock stock = stocks.get(position);
			holder.textViewCode.setText(stock.getCode());
			holder.textViewPrice.setText(String.format("%,.2f", stock.getPrice()));
			holder.textViewInfo.setText("Entry: " + String.format("%,.4f", stock.getEntryPrice()) );
			holder.textViewTargetPrice.setText("Target: " + String.format("%,.4f", stock.getTargetPrice()) + " " +String.format("%,.2f", stock.getTargetTotal()));
			holder.textViewTargetPercent.setText(String.format("%.2f", stock.getTargetPercent()) + "%");
			holder.textViewCutlossPrice.setText("Cutloss: " + String.format("%,.4f", stock.getCutlossPrice()) + " " + String.format("%,.2f", stock.getCutlossTotal()));
			holder.textViewCutlossPercent.setText(String.format("%.2f", stock.getCutlossPercent()) + "%");
			holder.textViewChangePrice.setText("Change: " + String.format("%,.2f", stock.getChangeTotal()));
			holder.textViewChangePercent.setText(String.format("%,.2f", stock.getChangePercent()) + "%");

			if (stock.getChangePercent() < 0) {
				holder.textViewChangePercent.setTextColor(Color.parseColor("#CA3838")); // red
			}
			else {
				holder.textViewChangePercent.setTextColor(Color.parseColor("#319624")); // green
			}

			if (stock.getPrice() > 0.0) {
				if (stock.getPrice() >= stock.getTargetPrice() && stock.getTargetPrice() > 0.0) {
					//holder.textViewTargetPercent.setTextColor(Color.parseColor("#319624")); // green
					holder.card.setCardBackgroundColor(Color.parseColor("#e4f9e3"));
				}
				if (stock.getPrice() <= stock.getCutlossPrice() && stock.getCutlossPrice() > 0.0) {
					//holder.textViewCutlossPercent.setTextColor(Color.parseColor("#CA3838")); // red
					holder.card.setCardBackgroundColor(Color.parseColor("#f9e3e3"));
				}
			}
		}

		@Override
		public int getItemCount() {
			return stocks.size();
		}
    }

    class GetDevTask extends AsyncTask<Void, Void, ArrayList<Stock>> {

		private final WeakReference<Activity> activityWeakRef;

		public GetDevTask(Activity context) {
			this.activityWeakRef = new WeakReference<Activity>(context);
		}

		@Override
		protected ArrayList<Stock> doInBackground(Void... arg0) {
			StockModel db = new StockModel(activity);
			ArrayList<Stock> stockList = db.getAllStocks();
			db.close();
			return stockList;
		}

		@Override
		protected void onPostExecute(ArrayList<Stock> stockList) {
			if (activityWeakRef.get() != null && !activityWeakRef.get().isFinishing()) {
				//if (stocks == null || stocks.size() != stockList.size()) {
					stocks = stockList;
				//}
				if (stocks.size() != 0 && stocks != null) {
					contentAdapter = new ContentAdapter(recyclerView.getContext(), stocks);
			        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
			        recyclerView.setAdapter(contentAdapter);
			        registerForContextMenu(recyclerView);

					new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks.json");
				} else {
					Toast.makeText(activity, "No Device Found @ DB", Toast.LENGTH_SHORT).show();
				}
				progressDialog.dismiss();
			}
		}
	}

	class GetStockDataTask extends AsyncTask<String, Void, String> {
		protected String doInBackground(String... args) {
			StringBuffer response = new StringBuffer();
	        try {
				URL url = new URL(args[0]);

				if (args[0].startsWith("https://")) {
					HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

					// Create the SSL connection
					SSLContext sc;
					sc = SSLContext.getInstance("TLS");
					//sc.init(null, null, new java.security.SecureRandom());
					sc.init(
		            	null,
	            		new TrustManager[] { // accept all
							new X509TrustManager() {
								public void checkClientTrusted(X509Certificate[] chain, String authType) {}
								public void checkServerTrusted(X509Certificate[] chain, String authType) {}
								public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[]{}; }
							}
	            		}, 
	            		null
					);
					conn.setSSLSocketFactory(sc.getSocketFactory());
					conn.setHostnameVerifier(new HostnameVerifier() {
						@Override
						public boolean verify(String hostname, SSLSession session) {
							// accept all
							return true;
						}
		            });
		            //HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

					conn.setRequestMethod("GET");

					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine;

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
	            }
				else {
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();

					conn.setRequestMethod("GET");

					//responseCode = conn.getResponseCode();
					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine;

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
	            }
			} catch (Exception e) {
				//this.exception = e;
				e.printStackTrace();
			}
			return response.toString();
		}
	    protected void onPostExecute(String data) {
			if (data != null) {
				try {
					JSONObject obj_data = new JSONObject(data);

					//String as_of = obj_data.getString("as_of");

					JSONArray arr_stocks  = obj_data.getJSONArray("stock");
					//JSONObject obj_stock = arr_stocks.getJSONObject(0);

		    		boolean beep_flag = false;
					for (int i = 0; i < arr_stocks.length(); i++) {
						JSONObject obj_stock = arr_stocks.getJSONObject(i);
						//String name = obj_stock.getString("name");

						JSONObject obj_price = obj_stock.getJSONObject("price");
						String amount = obj_price.getString("amount");
						//String percent_change = obj_stock.getString("percent_change");
						//String volume = obj_stock.getString("volume");
						String symbol = obj_stock.getString("symbol");

						//String value = result.getString("value");

						for (Stock stock : stocks) {
							if (stock.getCode().equals(symbol)) {
					    		stock.setPrice(Double.valueOf(amount));

					    		if (stock.getPrice() > 0.0) {
									if (stock.getPrice() >= stock.getTargetPrice() && stock.getTargetPrice() > 0.0) {
										beep_flag = true;
									}
									if (stock.getPrice() <= stock.getCutlossPrice() && stock.getCutlossPrice() > 0.0) {
										beep_flag = true;
									}
					        	}
							}
						}
					}

					SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
					boolean beep_enable = sharedPrefs.getBoolean("SoundEnable", true);
					//Toast.makeText(activity, "refresh ui", Toast.LENGTH_SHORT).show();
		    		if (beep_flag && beep_enable) {
			        	try {
			        	    //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			        		Uri notification =  Uri.parse(sharedPrefs.getString("SoundTone", ""));
			        	    Ringtone r = RingtoneManager.getRingtone(activity.getApplicationContext(), notification);
			        	    r.play();
			        	} catch (Exception e) {
			        	    e.printStackTrace();
			        	}					        
		    		}

					contentAdapter.notifyDataSetChanged();

			    	//int unit = Integer.valueOf(sharedPrefs.getString("pref_query_method", "1"));
					final int duration = Integer.valueOf(sharedPrefs.getString("pref_refresh_duration", "5")) * 1000;
					if (duration > 0) {
					    handler.postDelayed(new Runnable() {
					        public void run() {
					        	new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks.json");
					        }
					    }, duration);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateView() {
		handler.removeCallbacksAndMessages(null);
		new GetDevTask(activity).execute();
	}
}
