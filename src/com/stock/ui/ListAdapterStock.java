package com.stock.ui;

import java.util.List;

import com.stock.db.Stock;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListAdapterStock extends ArrayAdapter<Stock> {

	private Context context;
	List<Stock> stocks;

	public ListAdapterStock(Context context, List<Stock> stocks) {
		super(context, R.layout.item_list, stocks);
		this.context = context;
		this.stocks = stocks;
	}

	private class ViewHolder {
		TextView textView1;
		TextView textView2;
		TextView textView3;
		TextView textView4;
	}

	@Override
	public int getCount() {
		return stocks.size();
	}

	@Override
	public Stock getItem(int position) {
		return stocks.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_list, parent, false);
			holder = new ViewHolder();

			holder.textView1 = (TextView) convertView
					.findViewById(R.id.textView1);
			holder.textView2 = (TextView) convertView
					.findViewById(R.id.textView2);
			holder.textView3 = (TextView) convertView
					.findViewById(R.id.textView3);
			holder.textView4 = (TextView) convertView
					.findViewById(R.id.textView4);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Stock stock = (Stock) getItem(position);

		holder.textView1.setText(stock.getCode());
		holder.textView2.setText("EP: " + String.format("%,.4f", stock.getEntryPrice()) + " TP: " + String.format("%,.4f", stock.getTargetPrice())+ " CP: " + String.format("%,.4f", stock.getCutlossPrice()));
		//holder.textView2.setText("BC: " + String.format("%,.4f", buy_charges) + " SC: " + String.format("%,.4f", sell_charges)+ " TP: " + String.format("%,.4f", (buy_base_price + buy_charges)));
		holder.textView3.setText("P: " + String.format("%,.4f", stock.getPrice()) + " AP: " + String.format("%,.4f", stock.getBuyAvg()));
		holder.textView4.setText("Gain/Loss: (" + String.format("%.2f", stock.getChangePercent()) + "%) " + String.format("%,.2f", stock.getChangeTotal()));
		//holder.textView3.setText(sim.getBalanceExpire()+" P"+String.format("%.2f", Double.valueOf(sim.getBalance())));

		return convertView;
	}

	@Override
	public void add(Stock stock) {
		stocks.add(stock);
		notifyDataSetChanged();
		super.add(stock);
	}

	@Override
	public void remove(Stock stock) {
		stocks.remove(stock);
		notifyDataSetChanged();
		super.remove(stock);
	}
}