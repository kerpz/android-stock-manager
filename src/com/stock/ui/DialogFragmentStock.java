package com.stock.ui;

import java.util.Locale;

import com.stock.db.Stock;
import com.stock.db.StockModel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

public class DialogFragmentStock extends DialogFragment {

	EditText editTextCode;
	EditText editTextEntryPrice;
	EditText editTextTargetPrice;
	EditText editTextCutlossPrice;
	EditText editTextShares;
	LinearLayout submitLayout;
	Stock stock;
	String tag;
	
	//public static final String ARG_ITEM_ID = "stock_edit_dialog_fragment";

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View customDialogView = inflater.inflate(R.layout.fragment_dialog_stock, (ViewGroup) getView(), false);
		builder.setView(customDialogView);

		editTextCode = (EditText) customDialogView.findViewById(R.id.editTextCode);
		editTextEntryPrice = (EditText) customDialogView.findViewById(R.id.editTextEntryPrice);
		editTextTargetPrice = (EditText) customDialogView.findViewById(R.id.editTextTargetPrice);
		editTextCutlossPrice = (EditText) customDialogView.findViewById(R.id.editTextCutlossPrice);
		editTextShares = (EditText) customDialogView.findViewById(R.id.editTextShares);
		submitLayout = (LinearLayout) customDialogView
				.findViewById(R.id.layout_submit);
		submitLayout.setVisibility(View.GONE);
		
		tag = this.getTag();
		
		if (tag.equals("Add")) {
			stock = new Stock();
		}
		else { // Edit
			Bundle bundle = this.getArguments();
			stock = bundle.getParcelable("selectedItem");
			editTextCode.setText(stock.getCode());
			editTextEntryPrice.setText(String.format("%.4f", stock.getEntryPrice()));
			editTextTargetPrice.setText(String.format("%.4f", stock.getTargetPrice()));
			editTextCutlossPrice.setText(String.format("%.4f", stock.getCutlossPrice()));
			editTextShares.setText(Long.toString(stock.getShares()));
		}

		builder.setTitle(this.getTag() + " Stock");
		builder.setCancelable(false);
		builder.setPositiveButton("Save",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// https://www.androidhive.info/2015/09/android-material-design-floating-labels-for-edittext/
						double val = 0.0000;
						long val2 = 0;

						if (!editTextCode.getText().toString().equals("")) {
							stock.setCode(editTextCode.getText().toString().toUpperCase(Locale.getDefault()));
							val = (editTextEntryPrice.getText().toString().equals("")) ? 0.0000 : Double.valueOf(editTextEntryPrice.getText().toString());
							stock.setEntryPrice(val);
							val = (editTextTargetPrice.getText().toString().equals("")) ? 0.0000 : Double.valueOf(editTextTargetPrice.getText().toString());
							stock.setTargetPrice(val);
							val = (editTextCutlossPrice.getText().toString().equals("")) ? 0.0000 : Double.valueOf(editTextCutlossPrice.getText().toString());
							stock.setCutlossPrice(val);
							val2 = (editTextShares.getText().toString().equals("")) ? 0 : Long.parseLong(editTextShares.getText().toString());
							stock.setShares(val2);
						}
						
						StockModel db = new StockModel(getActivity()); 
						if (tag.equals("Add")) {
							db.addStock(stock);
						}
						else {
							db.updateStock(stock);
						}
						db.close();

						FragmentListStocks fragmentListStock = (FragmentListStocks) getFragmentManager()
								.findFragmentByTag("android:switcher:" + R.id.viewPager + ":1");
						if (fragmentListStock.isVisible()) {
							fragmentListStock.updateView();
						}

						FragmentCardStocks fragmentCardStock = (FragmentCardStocks) getFragmentManager()
								.findFragmentByTag("android:switcher:" + R.id.viewPager + ":0");
						if (fragmentCardStock.isVisible()) {
							fragmentCardStock.updateView();
						}
					}
				});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		AlertDialog alertDialog = builder.create();

		return alertDialog;
	}

}