package com.stock.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
 
 
public class ActivityMain extends AppCompatActivity {
	
	private static final int RESULT_SETTINGS = 1;

	//ViewPager viewPager;
	
	//ReceiverStatusUpdate receiverStatusUpdate;
	ReceiverStockUpdate receiverStockUpdate;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        // Show menu icon
        /*
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
        */
        
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //tabLayout.addTab(tabLayout.newTab().setText("Status"));
        //tabLayout.addTab(tabLayout.newTab().setText("Load"));
 
        /*
        viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        //viewPager.setOffscreenPageLimit(0);
        viewPager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(tabLayout));
        
        
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
 
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
 
            }
 
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
 
            }
        });
        */
        /*
    	receiverStatusUpdate = new ReceiverStatusUpdate();
		registerReceiver(receiverStatusUpdate, new IntentFilter("com.stock.status.action.REFRESH"));
    	receiverStockUpdate = new ReceiverStockUpdate();
		registerReceiver(receiverStockUpdate, new IntentFilter("com.stock.sim.action.REFRESH"));
		*/
        
    	//receiverStockUpdate = new ReceiverStockUpdate();
		//registerReceiver(receiverStockUpdate, new IntentFilter("com.stock.status.action.REFRESH"));
    }
 
    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentCardStocks(), "Card");
        adapter.addFragment(new FragmentListStocks(), "List");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<Fragment>();
        private final List<String> mFragmentTitleList = new ArrayList<String>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Intent i;
        //SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    	//int method = Integer.valueOf(sharedPrefs.getString("pref_query_method", "1"));

        switch (item.getItemId()) {
	        case R.id.action_preferences:
				i = new Intent(this, ActivityPreferences.class);
				startActivityForResult(i, RESULT_SETTINGS);
				return true;
	        default:
	            return super.onOptionsItemSelected(item);
        }
    }

	/*
    @Override
	public void onResume() {
	    super.onResume();
		registerReceiver(receiverUssd, new IntentFilter("com.ohmnismart.ussd.action.REFRESH"));
		registerReceiver(receiverSms, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
	}
	*/
	
	@Override
	public void onDestroy() {
	    super.onDestroy();              
	}

	/*
	public class ReceiverStatusUpdate extends BroadcastReceiver {
        @Override
		public void onReceive(Context context, Intent intent) {
			//final Bundle bundle = intent.getExtras();
            //String text = intent.getStringExtra("text");
        	FragmentCardStocks fragmentCardStatus = (FragmentCardStocks) getSupportFragmentManager()
					.findFragmentByTag("android:switcher:" + R.id.viewPager + ":0");
			if (fragmentCardStatus.isVisible()) {
				fragmentCardStatus.updateView();
			}
		}
	}
	*/

	public class ReceiverStockUpdate extends BroadcastReceiver {
        @Override
		public void onReceive(Context context, Intent intent) {
			//final Bundle bundle = intent.getExtras();
            //String text = intent.getStringExtra("text");
			FragmentCardStocks fragmentCardStock = (FragmentCardStocks) getSupportFragmentManager()
					.findFragmentByTag("android:switcher:" + R.id.viewPager + ":0");
			if (fragmentCardStock.isVisible()) {
				fragmentCardStock.updateView();
			}
		}
	}

}