package com.stock.ui;

import com.stock.db.Stock;
import com.stock.db.StockModel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
 
public class FragmentListStocks extends Fragment implements OnItemClickListener, OnItemLongClickListener {
	static Activity activity;
	ListView stockListView;
	ArrayList<Stock> stocks;

	ListAdapterStock stockListAdapter;
	Stock stock;

	private GetDevTask task;
	private SwipeRefreshLayout swipeLayout;
	//private ProgressDialog progressDialog;
	private FloatingActionButton fab;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = getActivity();
		//deviceDAO = new DeviceDAO(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_list, container, false);
		findViewsById(view);

		stockListView.setOnItemClickListener(this);
		stockListView.setOnItemLongClickListener(this);

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
    			//Bundle arguments = new Bundle();
    			DialogFragmentStock customDevDialogFragment = new DialogFragmentStock();
    			//customDevDialogFragment.setArguments(arguments);
    			customDevDialogFragment.show(getFragmentManager(), "Add");
            }
        });

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
		//swipeLayout.setColorSchemeResources(R.color.flat_button_text);
		swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            	updateView();
            }
        });

		return view;
	}

	private void findViewsById(View view) {
		stockListView = (ListView) view.findViewById(R.id.listViewDevices);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			//progressDialog = ProgressDialog.show(activity, "", "Scanning Devices via DB ...", true);
			//progressDialog.setCancelable(true);
			updateView();
		}
	}
   
	@Override
	public void onItemClick(AdapterView<?> list, View arg1, int position, long arg3) {
		stock = (Stock) list.getItemAtPosition(position);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> list, View view, int position, long arg3) {
		stock = (Stock) list.getItemAtPosition(position);
		if (stock != null) {
			//findViewsById(view);
			//list.showContextMenuForChild(deviceListView);
			list.showContextMenu();
		}
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
	    //menu.setHeaderTitle("Choose what to do");
	    menu.add(0, 0, 0, "Edit");
	    menu.add(0, 1 ,1, "Delete");
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
	    if (getUserVisibleHint()) {
            //SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
	    	//SmsManager smsManager = SmsManager.getDefault();

		    switch (menuItem.getItemId()) {
		        case 0:
		        	// Edit
					Bundle arguments = new Bundle();
					arguments.putParcelable("selectedItem", stock);
					DialogFragmentStock customDevDialogFragment = new DialogFragmentStock();
					customDevDialogFragment.setArguments(arguments);
					//customDevDialogFragment.show(getFragmentManager(), DialogFragmentEditStock.ARG_ITEM_ID);
					customDevDialogFragment.show(getFragmentManager(), "Edit");
		            break;
		        case 1:
		        	// Delete
					new AlertDialog.Builder(activity)
					//.setTitle("Title")
					.setMessage("Are you sure you want to delete?")
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							StockModel db = new StockModel(activity);
							db.deleteStock(stock);
							db.close();
							stockListAdapter.remove(stock);
						}})
					.setNegativeButton(android.R.string.no, null).show();
		            break;
		    }
		    return true;
	    }
	    return false;
	}
	
	class GetDevTask extends AsyncTask<Void, Void, ArrayList<Stock>> {

		private final WeakReference<Activity> activityWeakRef;

		public GetDevTask(Activity context) {
			this.activityWeakRef = new WeakReference<Activity>(context);
		}

		@Override
		protected ArrayList<Stock> doInBackground(Void... arg0) {
			StockModel db = new StockModel(activity);
			ArrayList<Stock> stockList = db.getAllStocks();
			db.close();
			return stockList;
		}

		@Override
		protected void onPostExecute(ArrayList<Stock> stockList) {
			if (activityWeakRef.get() != null && !activityWeakRef.get().isFinishing()) {
				//Log.d("devices", devList.toString());
				stocks = stockList;
				if (stockList != null) {
					if (stockList.size() != 0) {
						stockListAdapter = new ListAdapterStock(activity, stockList);
						stockListView.setAdapter(stockListAdapter);
						registerForContextMenu(stockListView);

						//for(Stock stock : stockList) {
						//	new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks/" + stock.getCode() + ".json");
						//}
						new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks.json");
					} else {
						Toast.makeText(activity, "No Device Found @ DB", Toast.LENGTH_SHORT).show();
					}
				}
				/*
        		if (swipeLayout.isRefreshing()) {
	            	swipeLayout.setRefreshing(false);
	            }
	            else {
	            	//progressDialog.dismiss();
	            }
				*/
			}
		}
	}

	class GetStockDataTask extends AsyncTask<String, Void, String> {
	    protected String doInBackground(String... args) {
            StringBuffer response = new StringBuffer();
	        try {
	            URL url = new URL(args[0]);

	            if (args[0].startsWith("https://")) {
		            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

		            // Create the SSL connection
		            SSLContext sc;
		            sc = SSLContext.getInstance("TLS");
		            //sc.init(null, null, new java.security.SecureRandom());
		            sc.init(
		            	null,
	            		new TrustManager[] { // accept all
							new X509TrustManager() {
								public void checkClientTrusted(X509Certificate[] chain, String authType) {}
								public void checkServerTrusted(X509Certificate[] chain, String authType) {}
								public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[]{}; }
							}
	            		}, 
	            		null
		            );
		            conn.setSSLSocketFactory(sc.getSocketFactory());
		            conn.setHostnameVerifier(new HostnameVerifier() {
						@Override
						public boolean verify(String hostname, SSLSession session) {
							// accept all
							return true;
						}
		            });
		            //HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		            conn.setRequestMethod("GET");

		            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            String inputLine;

		            while ((inputLine = in.readLine()) != null) {
		                response.append(inputLine);
		            }
		            in.close();
	            }
	            else {
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		            conn.setRequestMethod("GET");

		            //responseCode = conn.getResponseCode();
		            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            String inputLine;

		            while ((inputLine = in.readLine()) != null) {
		                response.append(inputLine);
		            }
		            in.close();
	            }
	        } catch (Exception e) {
	            //this.exception = e;
	            e.printStackTrace();
	        }
        	return response.toString();
	    }
	    protected void onPostExecute(String data) {
	    	if (data != null) {
	    		try {
		    		JSONObject obj_data = new JSONObject(data);
		    		
		    		//String as_of = obj_data.getString("as_of");

		    		JSONArray arr_stocks  = obj_data.getJSONArray("stock");
		    		//JSONObject obj_stock = arr_stocks.getJSONObject(0);

					for (int i = 0; i < arr_stocks.length(); i++) {
						JSONObject obj_stock = arr_stocks.getJSONObject(i);
			    		//String name = obj_stock.getString("name");

						JSONObject obj_price = obj_stock.getJSONObject("price");
			    		String amount = obj_price.getString("amount");
			    		//String percent_change = obj_stock.getString("percent_change");
			    		//String volume = obj_stock.getString("volume");
			    		String symbol = obj_stock.getString("symbol");

			    		//String value = result.getString("value");

						for (Stock stock : stocks) {
							//new GetStockDataTask().execute("http://phisix-api3.appspot.com/stocks/" + stock.getCode() + ".json");
							if (stock.getCode().equals(symbol)) {
								//Toast.makeText(activity, String.format("%.2f", stock.getPrice()), Toast.LENGTH_SHORT).show();
					    		stock.setPrice(Double.valueOf(amount));
							}
						}
					}

					stockListAdapter.notifyDataSetChanged();

					if (swipeLayout.isRefreshing()) {
		            	swipeLayout.setRefreshing(false);
		            }
		            else {
		            	//progressDialog.dismiss();
		            }
	            } catch (Exception e) {
					if (swipeLayout.isRefreshing()) {
		            	swipeLayout.setRefreshing(false);
		            }
		            else {
		            	//progressDialog.dismiss();
		            }
	                e.printStackTrace();
	            }
	    	}
	    }
	}

	/*
	 * This method is invoked from MainActivity onFinishDialog() method. It is
	 * called from CustomEmpDialogFragment when an employee record is updated.
	 * This is used for communicating between fragments.
	 */
	public void updateView() {
		task = new GetDevTask(activity);
		task.execute((Void) null);
	}
}